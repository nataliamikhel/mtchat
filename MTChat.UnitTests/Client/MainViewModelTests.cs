﻿using System;
using NSubstitute;
using NUnit.Framework;
using MTChat.Client.ViewModels;
using MTChat.Client.Services;
using MTChat.Common;
using MTChat.Common.Messages;
using MvvmDialogs;

namespace Tests.Client
{
    [TestFixture]
    public class MainViewModelTests
    {
        private MainViewModel _mainViewModel;
        private ILocalChatService _localChatService;
        private IDialogService _dialogService;

        [SetUp]
        public void Setup()
        {
            _localChatService = Substitute.For<ILocalChatService>();
            _dialogService = Substitute.For<IDialogService>();
            _mainViewModel = new MainViewModel(_localChatService, _dialogService);
            _mainViewModel.Chat = "";
        }

        #region Callback methods tests

        [TestCase(true)]
        [TestCase(false)]
        public void ReceiveTest(bool isWispered)
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var message = "Какой-то текст";
            var result = GetResulChatLine($"{person.Name}{(isWispered ? " шепнул" : "")}: {message}");

            // Act
            _mainViewModel.Receive(person, message, isWispered);

            // Assert
            AssertChatLine(result);
        }

        [Test]
        public void UserEnterTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var result = GetResulChatLine($"{person.Name} зашел в чат");

            // Act
            _mainViewModel.UserEnter(person);

            // Assert
            Assert.AreEqual(_mainViewModel.Persons.Count, 1);
            Assert.IsTrue(_mainViewModel.Persons[0].Equals(person));
            AssertChatLine(result);
        }

        [Test]
        public void UserLeaveTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            _mainViewModel.UserEnter(person);
            _mainViewModel.Chat = "";
            var result = GetResulChatLine($"{person.Name} покинул чат");

            // Act
            _mainViewModel.UserLeave(person);

            // Assert
            Assert.AreEqual(_mainViewModel.Persons.Count, 0);
            AssertChatLine(result);
        }

        #endregion // Callback methods tests

        #region SendMessage tests

        [Test]
        public void SendMessageToAllTest()
        {
            // Arrange
            var textMessage = new TextMessage(null, null);
            var personalTextMessage = new PersonalTextMessage(null, _mainViewModel.SelectedPerson, null);
            _mainViewModel.SelectedPerson = null;

            // Act
            _mainViewModel.SendMessage();

            // Assert
            _localChatService.ReceivedWithAnyArgs().Say(textMessage);
            _localChatService.DidNotReceiveWithAnyArgs().Whisper(personalTextMessage);
        }

        [Test]
        public void SendMessageToPersonTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var textMessage = new TextMessage(null, null);
            var personalTextMessage = new PersonalTextMessage(null, _mainViewModel.SelectedPerson, null);
            _mainViewModel.SelectedPerson = person;

            // Act
            _mainViewModel.SendMessage();

            // Assert
            _localChatService.DidNotReceiveWithAnyArgs().Say(textMessage);
            _localChatService.ReceivedWithAnyArgs().Whisper(personalTextMessage);
            _dialogService.DidNotReceiveWithAnyArgs().ShowMessageBox(_mainViewModel, "test", "ошибка");
        }

        [Test]
        public void SendMessageToAllFailedTest()
        {
            // Arrange
            var textMessage = new TextMessage(null, null);
            var personalTextMessage = new PersonalTextMessage(null, _mainViewModel.SelectedPerson, null);
            _mainViewModel.SelectedPerson = null;
            _localChatService.When(x => x.Say(Arg.Any<TextMessage>())).Do(x => { throw new Exception(); });

            // Act
            _mainViewModel.SendMessage();

            // Assert
            _localChatService.ReceivedWithAnyArgs().Say(textMessage);
            _localChatService.DidNotReceiveWithAnyArgs().Whisper(personalTextMessage);
            _dialogService.ReceivedWithAnyArgs().ShowMessageBox(_mainViewModel, "test", "ошибка");
        }

        [Test]
        public void SendMessageToPersonFailedTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var textMessage = new TextMessage(null, null);
            var personalTextMessage = new PersonalTextMessage(null, _mainViewModel.SelectedPerson, null);
            _mainViewModel.SelectedPerson = person;
            _localChatService.When(x => x.Whisper(Arg.Any<PersonalTextMessage>())).Do(x => { throw new Exception(); });

            // Act
            _mainViewModel.SendMessage();

            // Assert
            _localChatService.DidNotReceiveWithAnyArgs().Say(textMessage);
            _localChatService.ReceivedWithAnyArgs().Whisper(personalTextMessage);
            _dialogService.ReceivedWithAnyArgs().ShowMessageBox(_mainViewModel, "test", "ошибка");
        }

        #endregion // SendMessage tests

        #region OnChatCallbackEvent tests

        [Test]
        public void OnChatCallbackEventReceiveTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var message = "Какой-то текст";
            var e = new ProxyCallbackEventArgs
            {
                CallbackType = CallbackType.Receive,
                Person = person,
                Message = message
            };
            var result = GetResulChatLine($"{person.Name}: {message}");

            // Act
            _mainViewModel.OnChatCallbackEvent(null, e);

            // Assert
            AssertChatLine(result);
        }

        [Test]
        public void OnChatCallbackEventReceiveWhisperTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var message = "Какой-то текст";
            var e = new ProxyCallbackEventArgs
            {
                CallbackType = CallbackType.ReceiveWhisper,
                Person = person,
                Message = message
            };
            var result = GetResulChatLine($"{person.Name} шепнул: {message}");

            // Act
            _mainViewModel.OnChatCallbackEvent(null, e);

            // Assert
            AssertChatLine(result);
        }

        [Test]
        public void OnChatCallbackEventUserEnterTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var e = new ProxyCallbackEventArgs
            {
                CallbackType = CallbackType.UserEnter,
                Person = person
            };
            var result = GetResulChatLine($"{person.Name} зашел в чат");

            // Act
            _mainViewModel.OnChatCallbackEvent(null, e);

            // Assert
            Assert.AreEqual(_mainViewModel.Persons.Count, 1);
            Assert.IsTrue(_mainViewModel.Persons[0].Equals(person));
            AssertChatLine(result);
        }

        [Test]
        public void OnChatCallbackEventUserLeaveTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var e = new ProxyCallbackEventArgs
            {
                CallbackType = CallbackType.UserLeave,
                Person = person
            };
            _mainViewModel.UserEnter(person);
            _mainViewModel.Chat = "";
            var result = GetResulChatLine($"{person.Name} покинул чат");

            // Act
            _mainViewModel.OnChatCallbackEvent(null, e);

            // Assert
            Assert.AreEqual(_mainViewModel.Persons.Count, 0);
            AssertChatLine(result);
        }

        [Test]
        public void OnChatCallbackEventDisconnectByTimeoutTest()
        {
            // Arrange
            var person = new Person() { Name = "Иван" };
            var e = new ProxyCallbackEventArgs
            {
                CallbackType = CallbackType.DisconnectByTimeout,
                Person = person
            };
            _mainViewModel.UserEnter(person);
            _mainViewModel.Chat = "";
            var result = GetResulChatLine("Отключение по таймауту");

            // Act
            _mainViewModel.OnChatCallbackEvent(null, e);

            // Assert
            Assert.AreEqual(_mainViewModel.Persons.Count, 0);
            _localChatService.Received().Leave();
            AssertChatLine(result);
        }

        #endregion // OnChatCallbackEvent tests

        private void AssertChatLine(string resultLine)
        {
            Assert.NotZero(_mainViewModel.Chat.Length);
            Assert.AreEqual(_mainViewModel.Chat, resultLine);
        }

        private string GetResulChatLine(string message)
        {
            return $"({DateTime.Now.ToString("HH:mm:ss")}) {message} {Environment.NewLine}";
        }
    }
}